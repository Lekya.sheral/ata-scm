package main

import (
	//"bytes"
	"encoding/json"
	"fmt"
	//"strconv"
	//"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// SmartContract struct
type SmartContract struct {
}

//FarmerTransaction struct
type FarmerTransaction struct {
	//ObjectType               string `json:"docType"`
	//TxID                       string `json:tx_id`
	AssetID					   string `json:asset_id`
	ProductionDate			   string `json:production_date`
	TotalWeight                string `json:weight`
	FarmID                     string `json:farmID`
}

//ProcessorTransaction struct
type ProcessorTransaction struct {
	//TxID                       string `json:tx_id`
	AssetID					   string `json:asset_id`
	ReceivedDate			   string `json:received_date`
	TotalWeight                string `json:weight`
	ProcessorID                string `json:processorID`	
}


//FrAsset map variable
var FrAsset map[string]string

//PoAsset map variable
var PoAsset map[string]string

func init() {
	FrAsset = make(map[string]string)
	PoAsset = make(map[string]string)
}

// Init function
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

// Invoke function
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger
	if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "recordFarmerTransaction" {
		return s.recordFarmerTransaction(APIstub, args)
	} else if function == "recordProcessorTransaction" {
		return s.recordProcessorTransaction(APIstub, args)
	} else if function == "queryAssetID" {
		return s.queryAssetID(APIstub, args)
	} else if function == "initAssetID" {
		return s.initAssetID(APIstub)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

/*
 * The initLedger method *
Will add test data (10 asset catches)to our network
*/
func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {

	farmertx := []FarmerTransaction{
		FarmerTransaction{AssetID: "1201", ProductionDate: "03-06-2020", TotalWeight: "100", FarmID: "F001"},
	}
	processortx := []ProcessorTransaction{
		ProcessorTransaction{AssetID: "1201", ReceivedDate: "05-06-2020", TotalWeight: "50", ProcessorID: "P001"},
	}
	

	i := 0
	for i < len(farmertx) {
		fmt.Println("i is ", i)
		farmertxAsBytes, _ := json.Marshal(farmertx[i])
		APIstub.PutState(farmertx[i].AssetID, farmertxAsBytes)
		fmt.Println("Added", farmertx[i])
		i = i + 1
	}

	j := 0
	for j < len(processortx) {
		fmt.Println("j is ", j)
		processortxAsBytes, _ := json.Marshal(processortx[j])
		APIstub.PutState(processortx[j].AssetID, processortxAsBytes)
		fmt.Println("Added", processortx[j])
		j = j + 1
	}

	return shim.Success(nil)
}

/*
 * The initLedger method *
Will add test data (10 asset catches)to our network
*/
func (s *SmartContract) initAssetID(APIstub shim.ChaincodeStubInterface) sc.Response {

	FrAsset["id"] = "1200"
	asBytes, _ := json.Marshal(FrAsset)
	APIstub.PutState("FrAssetID", asBytes)

	PoAsset["1201"] = "3400"
	poasBytes, _ := json.Marshal(PoAsset)
	APIstub.PutState("PoAssetID", poasBytes)

	return shim.Success(nil)
}

/*
 * The query method *
 */
func (s *SmartContract) queryAssetID(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	assetAsBytes, _ := APIstub.GetState(args[0])
	if assetAsBytes == nil {
		return shim.Error("Could not locate asset")
	}

	return shim.Success(assetAsBytes)
}

/*
 * The recordFarmerTransaction method *
 */
func (s *SmartContract) recordFarmerTransaction(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}
	//objectType := "farmer"
	var farmertx = FarmerTransaction{AssetID: args[0], ProductionDate: args[1], TotalWeight: args[2], FarmID: args[3]}
	
	farmertxAsBytes, _ := json.Marshal(farmertx)
	err := APIstub.PutState(args[0], farmertxAsBytes)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to record farmertx catch: %s", args[0]))
	}

	frassetid := FrAsset
	assetAsBytes, _ := APIstub.GetState("FrAssetID")
	if assetAsBytes == nil {
		//shim.Error(fmt.Sprintf(FrAsset))
		return shim.Error("Could not locate asset")
	}
	json.Unmarshal(assetAsBytes, &frassetid)
	FrAsset["id"] = args[0]
	assetAsBytes, _ = json.Marshal(FrAsset)
	nerr := APIstub.PutState("FrAssetID", assetAsBytes)
	if nerr != nil {
		return shim.Error(fmt.Sprintf("error"))
	}

	return shim.Success(nil)
}

/*
 * The recordProcessorTransaction method *
 */
func (s *SmartContract) recordProcessorTransaction(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	var processortx = ProcessorTransaction{AssetID: args[0], ReceivedDate: args[1], TotalWeight: args[2], ProcessorID: args[3]}

	processortxAsBytes, _ := json.Marshal(processortx)
	err := APIstub.PutState(args[0], processortxAsBytes)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to record processortx catch: %s", args[0]))
	}

	poassetid := PoAsset
	assetAsBytes, _ := APIstub.GetState("PoAssetID")
	if assetAsBytes == nil {
		return shim.Error("Could not locate asset")
	}
	json.Unmarshal(assetAsBytes, &poassetid)
	PoAsset[args[1][0:4]] = args[1][4:]
	assetAsBytes, _ = json.Marshal(PoAsset)
	nerr := APIstub.PutState("PoAssetID", assetAsBytes)
	if nerr != nil {
		return shim.Error(fmt.Sprintf("error"))
	}

	return shim.Success(nil)
}

func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
