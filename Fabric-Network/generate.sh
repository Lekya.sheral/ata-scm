#!/bin/sh

export PATH=../bin:$PATH
export FABRIC_CFG_PATH=$PWD
export CHANNEL_NAME=chnv1

# remove previous crypto material and config transactions
rm -rf channel-artifacts/* 
rm -rf crypto-config/*

# generate crypto material
cryptogen generate --config=crypto-config.yaml
if [ "$?" -ne 0 ]; then
  echo "Failed to generate crypto material..."
  exit 1
fi

# echo "Generate CCP files for Farmer and Prcoessor"
# ./ccp-generate.sh


# generate genesis block for orderer
configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block
if [ "$?" -ne 0 ]; then
  echo "Failed to generate orderer genesis block..."
  exit 1
fi

# generate channel configuration transaction
configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID $CHANNEL_NAME
if [ "$?" -ne 0 ]; then
  echo "Failed to generate channel configuration transaction..."
  exit 1
fi

# generate anchor peer transaction
configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/FarmerMSPanchors.tx -channelID $CHANNEL_NAME -asOrg FarmerMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for FarmerMSP..."
  exit 1
fi

# generate anchor peer transaction
configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/ProcessorMSPanchors.tx -channelID $CHANNEL_NAME -asOrg ProcessorMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for ProcessorMSP..."
  exit 1
fi

