#!/bin/bash


CHANNEL_NAME="chnv1"
DELAY="3"
TIMEOUT="10"
VERBOSE="false"
COUNTER=1
MAX_RETRY=10
echo "Channel name : "$CHANNEL_NAME

ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/ata.com/orderers/orderer.ata.com/msp/tlscacerts/tlsca.ata.com-cert.pem
PEER0_Farmer_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/Farmer.ata.com/peers/peer0.Farmer.ata.com/tls/ca.crt
PEER0_Processor_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/Processor.ata.com/peers/peer0.Processor.ata.com/tls/ca.crt

# Set OrdererOrg.Admin globals
setOrdererGlobals() {
  CORE_PEER_LOCALMSPID="OrdererMSP"
  CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/ata.com/orderers/orderer.ata.com/msp/tlscacerts/tlsca.ata.com-cert.pem
  CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/ata.com/users/Admin@ata.com/msp
}

setGlobals() {
  PEER=$1
  ORG=$2
  
  if [ $ORG -eq 1 ]; then
    CORE_PEER_LOCALMSPID="FarmerMSP"
    CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_Farmer_CA
    CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/Farmer.ata.com/users/Admin@Farmer.ata.com/msp
    if [ $PEER -eq 0 ]; then
      CORE_PEER_ADDRESS=peer0.Farmer.ata.com:7051
    else
      CORE_PEER_ADDRESS=peer1.Farmer.ata.com:7055
    fi
  elif [ $ORG -eq 2 ]; then
    CORE_PEER_LOCALMSPID="ProcessorMSP"
    CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_Processor_CA
    CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/Processor.ata.com/users/Admin@Processor.ata.com/msp
    if [ $PEER -eq 0 ]; then
      CORE_PEER_ADDRESS=peer0.Processor.ata.com:9051
    else
      CORE_PEER_ADDRESS=peer1.Processor.ata.com:9053
    fi

  if [ "$VERBOSE" == "true" ]; then
    env | grep CORE
  fi
  fi 
}

# /***************** verify the result of the end-to-end test ***************** /
verifyResult() {
  if [ $1 -ne 0 ]; then
    echo "!!!!!!!!!!!!!!! "$2" !!!!!!!!!!!!!!!!"
    echo "========= ERROR !!! FAILED to execute End-2-End Scenario ==========="
    echo
    exit 1
  fi
}

createChannel(){
    setGlobals 0 1

    echo "Channel name : "$CHANNEL_NAME

    peer channel create -o orderer.ata.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/ata.com/orderers/orderer.ata.com/msp/tlscacerts/tlsca.ata.com-cert.pem
    res=$?
    set +x
    cat log.txt
    verifyResult $res "Channel creation failed"
    echo "===================== Channel '$CHANNEL_NAME' created ===================== "
    echo
}

## Sometimes Join takes time hence RETRY at least 5 times
joinChannelWithRetry() {

    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG

    set -x
    peer channel join -b $CHANNEL_NAME.block >&log.txt
    res=$?
    set +x
    cat log.txt
    if [ $res -ne 0 -a $COUNTER -lt $MAX_RETRY ]; then
        COUNTER=$(expr $COUNTER + 1)
        echo "peer${PEER}.org${ORG} failed to join the channel, Retry after $DELAY seconds"
        sleep $DELAY
        joinChannelWithRetry $PEER $ORG
    else
        COUNTER=1
    fi
    verifyResult $res "After $MAX_RETRY attempts, peer${PEER}.org${ORG} has failed to join channel '$CHANNEL_NAME' "

}

# step 2 - join channel
joinChannel () {
	

    for org in 1 2; do
	    for peer in 0; do
        joinChannelWithRetry $peer $org
        echo "===================== peer${peer}.org${org} joined channel '$CHANNEL_NAME' ===================== "
        sleep $DELAY
        echo
	    done
	  done
 
}


# step 3 - update anchor peers
updateAnchorPeers() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG

    set -x
    peer channel update -o orderer.ata.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
    
    # peer channel update -o orderer.ata.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx >&log.txt
    res=$?
    set +x

    cat log.txt
    verifyResult $res "Anchor peer update failed"
    echo "===================== Anchor peers updated for org '$CORE_PEER_LOCALMSPID' on channel '$CHANNEL_NAME' ===================== "
    sleep $DELAY
    echo
}


# ------------------------------- PROGRAM STARTS HERE -------------------------------#
# Create channel
echo "Creating channel..."
createChannel

# Join all the peers to the channel
echo "Having all peers join the channel..."
joinChannel

## Set the anchor peers for each org in the channel
echo "Updating anchor peers for Prodcuer..."
updateAnchorPeers 0 1
echo "Updating anchor peers for Processor..."
updateAnchorPeers 0 2



