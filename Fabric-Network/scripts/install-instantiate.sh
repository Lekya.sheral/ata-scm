#!/bin/bash

# # import utils
# source utility-prog.sh



CHANNEL_NAME="chnv1"
CHAIN_CODE_NAME="chainv1"
DELAY="3"
TIMEOUT="10"
VERBOSE="false"
COUNTER=1
MAX_RETRY=10
CC_SRC_PATH="github.com/chaincode/"
echo "Channel name : "$CHANNEL_NAME






ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/ata.com/orderers/orderer.ata.com/msp/tlscacerts/tlsca.ata.com-cert.pem
PEER0_Farmer_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/Farmer.ata.com/peers/peer0.Farmer.ata.com/tls/ca.crt

PEER0_Processor_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/Processor.ata.com/peers/peer0.Processor.ata.com/tls/ca.crt




# Set OrdererOrg.Admin globals
setOrdererGlobals() {
  CORE_PEER_LOCALMSPID="OrdererMSP"
  CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/ata.com/orderers/orderer.ata.com/msp/tlscacerts/tlsca.ata.com-cert.pem
  CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/ata.com/users/Admin@ata.com/msp
}


setGlobals() {
  PEER=$1
  ORG=$2
  
  if [ $ORG -eq 1 ]; then
    CORE_PEER_LOCALMSPID="FarmerMSP"
    CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/Farmer.ata.com/users/Admin@Farmer.ata.com/msp
    if [ $PEER -eq 0 ]; then
      CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_Farmer_CA
      CORE_PEER_ADDRESS=peer0.Farmer.ata.com:7051
    fi
  elif [ $ORG -eq 2 ]; then
    CORE_PEER_LOCALMSPID="ProcessorMSP"
    CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/Processor.ata.com/users/Admin@Processor.ata.com/msp
    if [ $PEER -eq 0 ]; then
      CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_Processor_CA
      CORE_PEER_ADDRESS=peer0.Processor.ata.com:9051
    fi

  if [ "$VERBOSE" == "true" ]; then
    env | grep CORE
  fi
  fi 
}


# /***************** verify the result of the end-to-end test ***************** /
verifyResult() {
  if [ $1 -ne 0 ]; then
    echo "!!!!!!!!!!!!!!! "$2" !!!!!!!!!!!!!!!!"
    echo "========= ERROR !!! FAILED to execute End-2-End Scenario ==========="
    echo
    exit 1
  fi
}


installChaincode() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG
    VERSION=${3:-1.0}
    set -x
    peer chaincode install -n ${CHAIN_CODE_NAME} -v ${VERSION} -p ${CC_SRC_PATH} >&log.txt
    res=$?
    set +x
    cat log.txt
    verifyResult $res "Chaincode installation on peer${PEER}.org${ORG} has failed"
    echo "===================== Chaincode is installed on peer${PEER}.org${ORG} ===================== "
    echo
}

instantiateChaincode() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG
    VERSION=${3:-1.0}

    # ,'DistributorMSP.peer','GrocerMSP.peer'
    set -x
    peer chaincode instantiate -o orderer.ata.com:7050 --tls $ata --cafile $ORDERER_CA -C $CHANNEL_NAME -n ${CHAIN_CODE_NAME} -v 1.0 -c '{"function":"initLedger","Args":[""]}' -P "OR ('FarmerMSP.peer','ProcessorMSP.peer')" >&log.txt
    res=$?
    set +x
    cat log.txt
    verifyResult $res "Chaincode instantiation on peer${PEER}.org${ORG} on channel '$CHANNEL_NAME' failed"
    echo "===================== Chaincode is instantiated on peer${PEER}.org${ORG} on channel '$CHANNEL_NAME' ===================== "
    echo
}



echo "Installing chaincode on peer0.org1..."
installChaincode 0 1
echo "Install chaincode on peer0.org2..."
installChaincode 0 2

echo "Instantiating chaincode on peer0.org1..."
instantiateChaincode 0 1

echo "Instantiating chaincode on peer0.org2..."
instantiateChaincode 0 2

