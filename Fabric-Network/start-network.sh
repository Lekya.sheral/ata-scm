#!/bin/bash

# Exit on first error, print all commands.
set -ev


sudo chmod 666 /var/run/docker.sock

# bring down any previous running networks
docker-compose -f docker-compose.yaml down

if [ "$?" -ne 0 ]; then
  echo "Failed to generate crypto material..."
  
  exit 1
fi


# bring up the network
sudo docker-compose -f docker-compose.yaml up -d 

