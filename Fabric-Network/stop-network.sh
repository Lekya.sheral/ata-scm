#!/bin/bash
sudo chmod 666 /var/run/docker.sock

docker rm -f $(docker ps -aq)
docker rmi -f $(docker images | grep dev-peer[0-9] | awk '{print $3}')

docker network prune 

docker container prune

docker volume prune


# docker rmi -f $(docker images | grep dev-anchor | awk '{print $3}')