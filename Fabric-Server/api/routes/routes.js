const express = require('express');
const router = express.Router();
var registrations = require('./registrations');
var invoke = require('./invokes');
var query = require('./query');

/* 
    Purpose  : To register admin in the network
    Type     : POST
    Inputs   : None
    Response : standard
    DB Call  : INSERT 

*/
router.post('/register-admin/', (req, res, next) => {
    console.log(req.body)
    console.log(
        {
            "body":req.body,    
            "call":"register_admin"
        }
    );

    registrations.registerAdmin(req.body.admin_name,req.body.org_name).then(data=>{
        
        console.log("data=====>",data);
        
        res.status(200).json(
            {
                "doc":data,
                "msg": "TEST"
            }
        );

    
        
        })

    
});

/* 
    Purpose  : To Register User in the network
    Type     : POST
    Inputs   : None
    Response : standard
    DB Call  : INSERT 

*/
router.post('/register-user/', (req, res, next) => {
    console.log(
        {
            "body":req.body,
            "call":"register-user"
        }
    );

    registrations.registerUser(req.body.admin_name,req.body.user_name,req.body.user_role,req.body.org_name).then(data=>{
            
        console.log("data=====>",data);
        
        res.status(200).json(
            {
                "doc":data,
                "msg": "Registration Succesful"
            }
        );

  
    })

});

router.post('/invoke/', (req, res, next) => {
    console.log(
        {
            "body":req.body,
            "call":"invoke"
        }
    );

    invoke.common_invoke(req.body.chain_code_name,req.body.user_name,req.body.func_name,req.body.args).then(data=>{
        console.log("data=====>",data);       
        res.status(200).json(
                {
                    "doc":data,
                    "msg": "invoke successful"
                }
            );
    
    
    })


});

router.post('/queryAll/', (req, res, next) => {
    // console.log(
    //     {
    //         "body":req.body,
    //         "call":"query"
    //     }
    // );
    query.common_query(req.body.chain_code_name,req.body.user_name,"queryAllTransactions",req.body.args).then(data=>{
        
        console.log("data=====>",data.length);
        res.status(200).json(
            {
                "doc":data.response.length,
                "msg": "verify success"
            }
        );
    })
});


module.exports = router;